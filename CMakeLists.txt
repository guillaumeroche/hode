project(hode)

cmake_minimum_required(VERSION 3.0)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

include(FindPkgConfig)

set(CMAKE_CXX_FLAGS "-Wall -pedantic -MMD ${CMAKE_CXX_FLAGS}")

pkg_check_modules(SDL2 sdl2 REQUIRED)

include_directories(
  ${SDL2_INCLUDE_DIRS}
)

file(GLOB SRC *.cpp 3p/inih/ini.c 3p/libxbr-standalone/xbr.c)
list(FILTER SRC EXCLUDE REGEX "fs_android.cpp|system_psp.cpp|system_wii.cpp")
add_executable(${CMAKE_PROJECT_NAME}
  ${SRC}
)

target_link_libraries(${CMAKE_PROJECT_NAME}
  ${SDL2_STATIC_LIBRARIES}
)

if(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "mipsel")
  message("Will build for RG-350 devices")
  add_compile_definitions(__rg350__)
  set(FILE_LIST hode ${CMAKE_SOURCE_DIR}/res/hode.png ${CMAKE_SOURCE_DIR}/res/default.gcw0.desktop)
  add_custom_command(
    TARGET ${CMAKE_PROJECT_NAME}
    POST_BUILD
    COMMAND ${CMAKE_STRIP} ${CMAKE_PROJECT_NAME}
    COMMAND mksquashfs ${FILE_LIST} ${CMAKE_PROJECT_NAME}.opk -all-root -no-xattrs -noappend -no-exports
  )
endif()

