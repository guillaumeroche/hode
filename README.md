# Heart of Darkness for the RG-350
This is a port of [hode](http://cyxdown.free.fr/hode/) (Heart of Darkness Engine), made by **Gregory Montoir** for the RG-350 retro-gaming handheld. See the original [README.txt](README.txt) file for more details.

You need the original PC game files to play the game.

## Build
### Dependencies
In order to build this project for your RG-350, you need:
 - CMake
 - The RG-350 [SDK](https://github.com/od-contrib/buildroot-rg350-old-kernel/releases/download/2020-12-27/mipsel-rg350-linux-uclibc_sdk-buildroot.tar.gz)
 - (optional) sdl2, if you want to build for your host platform

### Instructions
 - Clone this repository
 - From the repository folder, run:
```
mkdir build-rg350
cd build-rg350
cmake .. -DCMAKE_TOOLCHAIN_FILE=<sdk-path>/mipsel-rg350-linux-uclibc_sdk-buildroot/usr/share/buildroot/toolchainfile.cmake
make
```
This will generate the `hode.opk` file.

To build a debug executable for your host platform:
```
mkdir build-debug
cd build-debug
cmake .. -DCMAKE_BUILD_TYPE=Debug
make
```

## Install
Copy the `hode.opk` file to `/media/data/apps/`.
Copy the original game files (`*.lvl`, `*.mst`, `*.sss`, `Setup.dat`, `hod.paf`) to `/media/home/.hode/`.

## Play
The controls are mapped the same way as in the PSX version.

 - A: fire
 - X: fire / big fire
 - B: jump
 - Y + movement: run

Movements and targeting can be done with the D-Pad or the left stick. To open the exit menu, use the select or start button.

## More details
 - This work is based on a recent `hode` version, and hopefully will be kept up-to-date as much as I can.
 - I avoided to alter the original code, to ensure it can still build and run on a desktop target to help debugging. All RG-350 specific modifications are conditionally compiled with a `__rg350__` preprocessor definition.
 - I added the rumble effect when the screen shakes.

## Known issues
See the open issues.

## Contribute
Contributions are welcome, but please ensure the following:
 - The code matches existing style
 - It is tested
 - It is well documented, at least in the commit messages

Thanks!

=)
